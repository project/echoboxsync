<?php

namespace Drupal\echobox_usersynchronisation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['echobox_usersynchronisation.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'echobox_usersynchronisation_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('echobox_usersynchronisation.settings');
    
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('Enter your Echobox API key here.'),
      '#default_value' => $config->get('api_key'),
    ];
    
    $form['endpoint_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Endpoint URL'),
      '#description' => $this->t('Enter the Echobox endpoint URL.'),
      '#default_value' => $config->get('endpoint_url'),
      '#maxlength' => 512, // Erhöhte maximale Eingabelänge
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret Key'),
      '#description' => $this->t('Enter the Echobox secret key here.'),
      '#default_value' => $config->get('secret_key'),
    ];

    $form['campaign_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Campaign ID'),
      '#description' => $this->t('Enter the Echobox campaign ID here.'),
      '#default_value' => $config->get('campaign_id'),
    ];
    
    $form['note'] = [
      '#type' => 'markup',
'#markup' => '<div style="font-size: 7px; margin-top: 20px;">// Hats off to Antke, Birgit, Lisa, Andy, Antoine, Ed, Gabriel and Tibor. Keep iterating. - Flo</div>',

    ];
    
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('echobox_usersynchronisation.settings')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('endpoint_url', $form_state->getValue('endpoint_url'))
      ->set('secret_key', $form_state->getValue('secret_key'))
      ->set('campaign_id', $form_state->getValue('campaign_id'))
      ->save();
  }
}
